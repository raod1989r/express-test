var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/reports', function(req, res, next){
  var reports = [
    {name: 'first'},
    {name: 'second'}
  ];

  res.render('reports', {reports: reports});
});

module.exports = router;
